ARG NODE_VERSION=12
ARG DEBIAN_RELEASE=buster

FROM node:${NODE_VERSION}-${DEBIAN_RELEASE} AS builder

RUN mkdir -p /opt/prezbuilder
COPY . /opt/prezbuilder/

ENV NODE_ENV=production
RUN cd /opt/prezbuilder/ \
    && rm -rf src \
    && npm install

FROM node:${NODE_VERSION}-${DEBIAN_RELEASE}-slim

RUN apt-get update -yq \
    && apt-get install -yq --no-install-recommends \
    ca-certificates \
    fontconfig \
    libjpeg62-turbo \
    libx11-6 \
    libxcb1 \
    libxext6 \
    libxrender1 \
    wget \
    xfonts-75dpi \
    xfonts-base \
    && find /var/*/apt -type f -delete

ARG PANDOC_VERSION=2.9.2.1
RUN wget -nv -O /tmp/pandoc.tar.gz https://github.com/jgm/pandoc/releases/download/${PANDOC_VERSION}/pandoc-${PANDOC_VERSION}-linux-amd64.tar.gz \
    && cd /usr/local \
    && tar xfz /tmp/pandoc.tar.gz --strip-components=1 \
    && rm /tmp/pandoc.tar.gz \
    && pandoc --version

ARG WKHTMLTOPDF_VERSION=0.12.5
ARG DEBIAN_RELEASE=buster
RUN wget -nv -O /tmp/wkhtmltox.deb https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/${WKHTMLTOPDF_VERSION}/wkhtmltox_${WKHTMLTOPDF_VERSION}-1.${DEBIAN_RELEASE}_amd64.deb \
    && dpkg -i /tmp/wkhtmltox.deb \
    && rm /tmp/wkhtmltox.deb \
    && wkhtmltopdf --version

RUN mkdir -p /workdir/src /workdir/public /opt/prezbuilder
COPY --from=builder /opt/prezbuilder /opt/prezbuilder/
RUN ln -s /opt/prezbuilder/bin/prezbuilder /usr/local/bin/prezbuilder

ENV SERVER_HOST=0.0.0.0 \
    SERVER_PORT=3000 \
    WKHTMLTOPDF_BINARY=/usr/local/bin/wkhtmltopdf

WORKDIR /workdir
EXPOSE 3000/tcp 35729/tcp

ENTRYPOINT ["/usr/local/bin/prezbuilder"]
CMD ["dev"]
