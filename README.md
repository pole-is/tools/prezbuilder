# prezbuilder

Génération de présentations reveal.js.

Pour plus de détails : [consulter la présentation de prezbuilder](https://pole-is.gitlab.irstea.page/tools/prezbuilder/index.html).

Ou la [version textuelle](src/index.md).
