import { ASSET_GLOB, DEST_DIR, GRAPH_GLOB, PDF_GLOB, PREZ_GLOB, REVEALJS_URL } from './config';
import { dest, lastRun, parallel, series, src } from 'gulp';
import autoindex from './plugins/autoindex';
import del from 'del';
import drawio from './plugins/drawio';
import pandoc from './plugins/pandoc';
import revealjs from './revealjs';
import spy from './plugins/spy';
import wkhtmltopdf from './plugins/wkhtmltopdf';

export const clean = () => del(`${DEST_DIR}/**`, { force: true });

export const assets = () =>
  src(ASSET_GLOB, { since: lastRun(assets) }).pipe(dest(DEST_DIR)).pipe(spy());

export const prez = () =>
  src(PREZ_GLOB)
    .pipe(autoindex())
    .pipe(pandoc({ revealJSURL: REVEALJS_URL }))
    .pipe(dest(DEST_DIR)).pipe(spy());

export const graphs = () =>
  src(GRAPH_GLOB, { since: lastRun(graphs) })
    .pipe(drawio())
    .pipe(dest(DEST_DIR)).pipe(spy());

export const pdf = () => src(PDF_GLOB).pipe(wkhtmltopdf()).pipe(dest(DEST_DIR)).pipe(spy());

export const build = series(
  clean,
  parallel(revealjs, assets, prez, graphs),
  pdf
);

export default build;
