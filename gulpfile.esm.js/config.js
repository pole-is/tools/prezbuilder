import logger from "gulplog";
import { magenta } from "gulp-cli/lib/shared/ansi";

export const SRC_DIR = process.env.SRC || "src";
export const DEST_DIR = process.env.OUTPUT || "public";

logger.info(`Using input directory ${magenta(SRC_DIR)} ($SRC)`);
logger.info(`Using output directory ${magenta(DEST_DIR)} ($OUTPUT)`);

export const REVEALJS_URL = process.env.REVEALJS_URL || "reveal.js";

export const ASSET_GLOB = [
  `${SRC_DIR}/**/*.{png,gif,jpg,svg}`,
  __dirname + "/../assets/**/*",
];

export const PREZ_GLOB = `${SRC_DIR}/**/index.md`;
export const GRAPH_GLOB = `${SRC_DIR}/**/*.drawio`;
export const PDF_GLOB = `${DEST_DIR}/**/index.html`;
