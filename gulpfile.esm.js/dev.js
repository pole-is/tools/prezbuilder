import { ASSET_GLOB, DEST_DIR, GRAPH_GLOB, PREZ_GLOB } from './config';
import { assets, build, graphs, pdf, prez } from './build';
import { parallel, series, src, watch } from 'gulp';
import revealjs from './revealjs';
import server from 'gulp-server-livereload';

const serve = () => {
  src(DEST_DIR).pipe(
    server({
      host: process.env.SERVER_HOST || "localhost",
      port: process.env.SERVER_PORT || 3000,
      livereload: {
        enable: true,
        filter: (filename, cb) => cb(!/(node_modules|\.pdf)$/.test(filename))
      }
    })
  );
};

const watch_prez = () => watch([PREZ_GLOB, __dirname + "/templates/**"], prez);
const watch_assets = () => watch(ASSET_GLOB, assets);
const watch_graphs = () => watch(GRAPH_GLOB, graphs);
const watch_pdf = () => watch(`${DEST_DIR}/**/*.{html,svg,jpg,gif,png}`, pdf);

export const dev = series(
  build,
  revealjs,
  parallel(
    serve,
    watch_prez,
    watch_assets,
    watch_graphs,
    watch_pdf,
  )
);

export default dev;
