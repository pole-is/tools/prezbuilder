import { callbackify } from "util";
import File from "vinyl";
import { getPDFOutput } from "./wkhtmltopdf";
import logger from "gulplog";
import { obj } from "through2";
import PluginError from "plugin-error";

const PLUGIN_NAME = "autoindex";

/**
 * @param {ReadableStream} stream
 * @return {Promise<Buffer>}
 */
function streamToBuffer(stream) {
  const chunks = [];
  return new Promise((resolve, reject) => {
    stream.on("data", (chunk) => chunks.push(chunk));
    stream.once("end", () => resolve(Buffer.concat(chunks)));
    stream.once("error", reject);
  });
}

/**
 * @param {File} file
 * @return {Promise<string>}
 */
async function vinylToString(file) {
  if (file.isStream()) {
    const buffer = await streamToBuffer(file.contents);
    return buffer.toString();
  }
  if (file.isBuffer()) {
    return file.contents.toString();
  }
  return "";
}

const pandocTitleBlockRegExp = /^%( .+(?:\n {2}.+)*)?\n%( .+(?:\n {2}.+)*)?\n%( .+)?\n/u;

/**
 * @param {string} text
 * @return {object|null}
 */
function parsePandocTitleBlock(text) {
  const match = pandocTitleBlockRegExp.exec(text);
  if (!match) {
    return null;
  }
  return { title: match[1].trim() };
}

/**
 * @param {string} text
 * @return {object|null}
 */
function parseYamlTitleBlock(text) {
  if (!text.startsWith("---\n")) {
    return null;
  }
  throw new PluginError(
    PLUGIN_NAME,
    new Error("yaml title block is not supported"),
    {
      lineNumber: 1,
    }
  );
}

/**
 * @param {File} input
 * @return {string}
 */
function getPrezFilename(input) {
  const prez = input.clone({ deep: false, contents: false });
  prez.extname = ".html";
  return prez.relative;
}

function renderInput(input) {
  return `- [${input.titleBlock.title}](${getPrezFilename(input)})` +
    `[<img class="plain icon" src="file_pdf.png"/>](${getPDFOutput(input).relative})`;
}

function renderByTitleSlide(initials, inputs) {
  const groupTitle = initials.length === 1 ? initials[0] : `${initials[0]}-${initials[initials.length-1]}`;
  return `## ${groupTitle}\n\n` + inputs.map(renderInput).join("\n");
}

function groupByTitleInitial(inputs) {
  const byInitials = {};
  inputs.forEach((input) => {
    const title = input.titleBlock.title.toUpperCase();
    const letter = title[title.search(/[A-Z]/)];
    if (!byInitials[letter]) {
      byInitials[letter] = [input];
    } else {
      byInitials[letter].push(input);
    }
  });
  return byInitials;
}

function groupByTitle(inputs) {
  const byInitials = groupByTitleInitial(inputs);
  const initials = Object.keys(byInitials).sort();

  const groups = [];
  let group = '';
  let groupSlides = [];
  initials.forEach((initial) => {
    const slides = byInitials[initial];
    if (groupSlides.length + slides.length > 10) {
      groups.push([group, groupSlides]);
      group = '';
      groupSlides = [];
    }
    group += initial;
    groupSlides.push(...slides);
  });
  if (groupSlides.length > 0) {
    groups.push([group, groupSlides]);
  }

  return groups;
}

function renderIndex(inputs) {
  const groups = groupByTitle(inputs);
  const slides = groups.map(([group, slides]) => renderByTitleSlide(group, slides)).join("\n\n");

  return `% Présentations
% Dev@Science
% ${new Date().toLocaleDateString("fr-FR", {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
  })}

# Par titre
 <style>
 .reveal section img.icon {vertical-align:top;margin:0;height:1em}
 </style>

${slides}`;
}

export default function autoindex() {
  const inputs = [];
  return obj(
    callbackify(async function (input) {
      const text = await vinylToString(input);
      input.titleBlock = parsePandocTitleBlock(text) ||
        parseYamlTitleBlock(text) || { title: input.basename };
      inputs.push(input);
      this.push(input);
    }),
    callbackify(async function () {
      if (inputs.find((file) => file.relative === "index.md")) {
        logger.info(`${PLUGIN_NAME}: will not override existing index.md`);
        return;
      }
      inputs.sort((a, b) =>
        a.titleBlock.title.localeCompare(b.titleBlock.title)
      );
      const contents = renderIndex(inputs);
      this.push(
        new File({
          base: "/",
          path: "/index.md",
          contents: Buffer.from(contents),
        })
      );
      logger.info("%s: generated index.md", PLUGIN_NAME);
    })
  );
}
