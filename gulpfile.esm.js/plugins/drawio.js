import exec from "../util/exec";
import fs from "fs";
import logger from "gulplog";
import { mkTempFile } from "../util/tempdir";
import parallel from "../util/parallel";
import PluginError from "plugin-error";
import { withBinary } from "../util/optional";

const PLUGIN_NAME = "drawio";

const drawio = withBinary(
  "DRAWIO_BINARY",
  "drawio",
  (DRAWIO_BINARY) =>
    function drawio() {
      return parallel(async function (input) {
        try {
          const output = await mkTempFile(input);
          output.extname = ".svg";

          const args = [
            "--export",
            "--format",
            "svg",
            "--width",
            "1024",
            "--output",
            output.path,
            input.path,
          ];

          await exec(PLUGIN_NAME, DRAWIO_BINARY, args);

          logger.info(
            "%s: %s -> %s",
            PLUGIN_NAME,
            input.relative,
            output.relative
          );

          output.contents = fs.createReadStream(output.path, {
            encoding: "UTF-8",
          });
          this.push(output);
        } catch (error) {
          throw new PluginError(PLUGIN_NAME, error, { showStack: true });
        }
      });
    }
);

export default drawio;
