import logger from "gulplog";
import { obj } from "through2";

export default function spy(limit = 5) {
  let count = 0;
  return obj(function(file, _enc, next) {
    this.push(file);
    next();
    if (file.isDirectory() || ++count > limit) {
      return;
    }

    logger.info("%s -> %s", file.history[0], file.path);
  }, (next) => {
    next();
    if (count > limit) {
      logger.info("... and %d more files", count - 5);
    }
  });
}
