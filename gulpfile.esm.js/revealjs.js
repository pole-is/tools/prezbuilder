import { dest, lastRun, src } from 'gulp';
import { DEST_DIR, REVEALJS_URL } from './config';
import logger from 'gulplog';
import { magenta } from 'gulp-cli/lib/shared/ansi';
import path from 'path';
import spy from './plugins/spy';

let revealjs = () => Promise.resolve(false)

if (REVEALJS_URL.startsWith("http")) {
  logger.info(`Using remote reveal.js at ${magenta(REVEALJS_URL)} ($REVEALJS_URL)`);

} else {
  const _SRC_DIR = path.normalize(`${__dirname}/../node_modules/@devatscience/reveal.js`);
  const SRC = `${_SRC_DIR}/**`;
  const DST = path.normalize(DEST_DIR + "/" + REVEALJS_URL);

  logger.info(`Using local ${DEST_DIR}/${magenta(path.relative(DEST_DIR, DST))} from ${path.relative(__dirname + '/..', _SRC_DIR + '/..')} ($REVEALJS_URL)`);

  revealjs = () =>
    src([SRC, "!**/package.json"], { since: lastRun(revealjs) }).pipe(dest(DST)).pipe(spy());

}

export default revealjs;
