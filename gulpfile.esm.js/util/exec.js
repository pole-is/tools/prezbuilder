import * as colors from "gulp-cli/lib/shared/ansi";
import { execFile } from "child_process";
import logger from "gulplog";

/**
 *
 * @param {string} plugin_name
 * @param {string} cmd
 * @param {string[]} args
 * @param {import("child_process").ExecFileOptions} opts
 * @return {Promise<{ stdout: string|Buffer, stderr: string|Buffer }>}
 */
export default function exec(plugin_name, cmd, args, opts = {}) {
  return new Promise((resolve, reject) => {
    execFile(cmd, args, opts, (error, stdout, stderr) => {
      if (!error) {
        logger.debug("%s: %s terminated successfully", plugin_name, cmd);
        return resolve({ stdout, stderr });
      }
      logger.error("%s: %s failed: %s", plugin_name, cmd, colors.red(error.message));
      logger.error("%s: command: `%s %s`", plugin_name, cmd, args.join(" "));
      logger.error("%s: stdout:\n%s", plugin_name, stdout.toString());
      logger.error("%s: stderr:\n%s", plugin_name, stderr.toString());
      reject(error);
    });
  });
}
