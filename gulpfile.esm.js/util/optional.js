import logger from "gulplog";
import { magenta } from "gulp-cli/lib/shared/ansi";
import { obj } from "through2";
import which from "which";

function noop() {
  return obj((_file, _enc, done) => done());
}

/**
 *
 * @param {string} envName
 * @param {any}
 * @param {*} resolve
 */
export function withBinary(envName, execName, resolve) {
  const setting = process.env[envName] || execName;
  const binary = which.sync(setting, { nothrow: true });
  if (!binary) {
    logger.info("Not using %s: binary not found ($%s)", execName, envName);
    return noop;
  }
  logger.info(
    "Using %s %s ($%s)",
    execName,
    magenta(binary),
    envName
  );
  return resolve(binary);
}
