
import { cpus } from "os";
import { obj } from "through2";
import pMap from "p-map";

const concurrency = cpus().length;

/**
 *
 * @param {(file) => Promise<any>} mapper
 * @return {import('stream').Transform}
 */
export default function parallel(mapper) {
  const inputs = [];

  return obj(
    function collect(input, _enc, done) {
      inputs.push(input);
      done();
    },
    function mapAll(done) {
      pMap(inputs, mapper.bind(this), { concurrency, stopOnError: false }).then(
        () => done(),
        error => done(error)
      );
    }
  );
}
