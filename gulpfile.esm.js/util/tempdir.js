import del from "del";
import File from "vinyl";
import fs from "fs";
import logger from "gulplog";
import os from "os";
import { promisify } from "util";

const mkdtemp = promisify(fs.mkdtemp);
const mkdir = promisify(fs.mkdir);

let tmpDir = null;

const TMPDIR = process.env.TMPDIR || os.tmpdir();

/**
 * @return {Promise<string>}
 */
export default function getTempDir() {
  if (tmpDir === null) {
    tmpDir = mkdtemp(TMPDIR + "/prezbuilder-").then((path) => {
      path += "/";
      logger.debug("Created temporary directory `%s`", path);
      process.once("beforeExit", () => {
        logger.debug("Removing temporary directory `%s`", path);
        return del(path, { recursive: true, force: true });
      });
      return path;
    });
  }
  return tmpDir;
}

/**
 * @param {File} file
 * @return {Promise<File>}
 */
export async function mkTempFile(file) {
  const tmpDir = await getTempDir();
  const tmpFile = new File({
    history: file.history,
    base: tmpDir,
    path: tmpDir + file.relative,
  });
  await mkdir(tmpFile.dirname, { recursive: true, mode: 0o700 });
  return tmpFile;
}
