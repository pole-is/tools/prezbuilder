% prezbuilder master
% Guillaume Perréal
% 2020-05

# Présentation

- Génère des présentations [reveal.js](https://revealjs.com/) à partir de fichiers en markdown.
- Utilise le [thème INRAE](https://gitlab.irstea.fr/pole-is/tools/reveal.js).
- Génération de la présentation avec [pandoc](https://pandoc.org/).
- Conversion automatique de graphes créés avec [draw.io](https://app.diagrams.net/),
  _si drawio-desktop est installé_.
- Génération d'une version PDF avec [wkhtmltopdf](https://wkhtmltopdf.org/), _s'il est installé_.
- Support des graphes [mermaid.js](https://mermaid-js.github.io/) intégrés.
- Mode "dev" avec rechargement automatique de page.

La présentation que vous regardez a été générée avec prezbuilder !

# Installation

## Avec Docker

L'image gitlab-registry.irstea.fr/pole-is/tools/prezbuilder:master contient
une version préinstallée de prezbuilder avec pandoc et wkhtmltopdf, _mais pas
drawio_.

Il faut s'authentifier sur la registry Docker de Gitlab si ce n'est pas déjà fait :

```bash
docker login gitlab-registry.irstea.fr
```

Ensuite, le plus simple est de récupérer le script `prezbuilder-docker` :

```console
sudo wget https://gitlab.irstea.fr/pole-is/tools/prezbuilder/-/raw/master/bin/prezbuilder-docker?inline=false -O /usr/local/bin/prezbuilder-docker
sudo chmod a+rx /usr/local/bin/prezbuilder-docker
```

## Avec Gitlab Pages

Le projet contient un modèle Gitlab CI à inclure dans `.gitlab-ci.yml`.
Il définit un modèle de job `.prezbuilder` qui peut être utilisé
comme job `pages` pour publier automatiquement les présentations de `src`.
Il utilise l'image Docker, donc génère les PDF mais ne convertit pas les
graphes drawio.

```yaml
include:
  - project: pole-is/tools/prezbuilder
    ref: master
    file: prezbuilder-ci.yml

pages:
  extends: .prezbuilder
  # Exemple: n'exécute ce job que pour les tags.
  # cf. https://docs.gitlab.com/ce/ci/yaml/README.html#rules
  rules:
    - if: $CI_COMMIT_TAG
```

# Utilisation

prezbuild crée une (ou plusieurs) présentation(s) dans `public/` à partir du
contenu du répertoire `src/`.

Chaque présentation est écrite sous la forme d'un fichier au format Markdown
nommés `index.md`. Il est possible d'ajouter des images aux formats PNG, JPG, GIF ou SVG à référencer
depuis le fichier markdown.

## Organisation des fichiers

Le répertoire `src/` est parcouru récursivement, et les fichiers sont traités comme suit :

- Les fichiers `index.md` sont convertis en présentation `index.html`.
- Les fichiers `.png`, `.jpg`, `.gif` et `.svg` sont copiés tels quels.
- S'il n'y a pas de fichier `index.md` à la racine, un index des présentations est généré.
- _Si drawio est installé,_ les fichiers `.drawio` sont convertis en fichiers `.svg`.
- _Si wkhtmltopdf est installé_ un fichier `.pdf` est généré par fichier `index.md`, avec le nom du répetoire parent (en sortie).

## Exemple

<div style="display:flex;width:100%;align-items:center">

<div style="flex-grow: 1">
Entrée:

```
src\
    pres1\
        index.md
        graph.drawio
    pres2\
        index.md
        photo.jpg
```

</div>

<div style="margin: 1ex; font-size: 24pt">➩</div>

<div style="flex-grow: 1">
Sortie :

```
public\
    index.html
    public.pdf
    pres1\
        index.html
        pres1.pdf
        graph.svg
    pres2\
        index.html
        pres2.pdf
        photo.jpg
```

</div>

</div>

# Exécution

prezbuiler peut être exécuté selon deux modes :

- `build` : une seule exécution.
  - pour produire les fichiers de la présentation.
- `dev` : mode serveur avec live-reload,
  - présentation accessible sur http://localhost:3000,
  - mise à jour automatique sur modification des sources.

## Avec Docker

Le script pour Docker s'utilise comme celui présenté de NodeJS :

#### Build

```bash
prezbuilder-docker build src public
```

#### Dev

```bash
prezbuilder-docker dev src public
```

# Références

- [Documentation reveal.js](https://github.com/hakimel/reveal.js/#table-of-contents)
- [Documentation pandoc](https://pandoc.org/MANUAL.html)
